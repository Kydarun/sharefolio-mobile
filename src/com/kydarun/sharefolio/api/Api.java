package com.kydarun.sharefolio.api;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.json.JSONException;
import org.json.JSONObject;

import com.kydarun.sharefolio.R;
import com.kydarun.sharefolio.constant.ApplicationConstant;

import android.content.Context;
import android.net.Uri;
import android.util.Log;

public class Api {
	public static final String FRIEND = "friends.php";
	public static final String NOTIFICATION = "notification.php";
	public static final String COMPANY = "company.php";
	public static final String WALL = "wall.php";
	
	private Context context;
	
	public Api(Context context) {
		this.context = context;
	}
	
	public JSONObject postJSON(String modulePath, Map<String, String> params) {
		try {
			DefaultHttpClient client = new DefaultHttpClient();
			List<NameValuePair> param = new ArrayList<NameValuePair>();
			for (Map.Entry<String, String> entry : params.entrySet()) {
				param.add(new BasicNameValuePair(entry.getKey(), entry.getValue()));
			}
			
			String url = ApplicationConstant.DEBUG ? 
				context.getResources().getString(R.string.debug_url) + "api/" + modulePath : 
				context.getResources().getString(R.string.api_url) + "api/" + modulePath;
			HttpPost post = new HttpPost(Uri.parse(url).buildUpon().toString());
			post.setEntity(new UrlEncodedFormEntity(param));
			post.addHeader("Accept", "application/json");
			
			HttpResponse response = client.execute(post);
			HttpEntity entity = response.getEntity();
			
			String json = EntityUtils.toString(entity);
			
			Log.d(Api.class.getSimpleName(), json);
			
			JSONObject returnObj = new JSONObject(json);
			if (returnObj.getString("status").equals("ok")) {
				return returnObj;
			}
		}
		catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (ClientProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
			e.printStackTrace();
		}
		
		return new JSONObject();
	}
}
