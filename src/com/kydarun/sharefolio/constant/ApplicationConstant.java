package com.kydarun.sharefolio.constant;

public class ApplicationConstant {
	public static final boolean DEBUG = true;
	
	/* DEBUG ONLY */
	public static final int DEBUG_ACCOUNT_ID = 363;
	public static final String DEBUG_ACCOUNT_AVATAR = ".png";
	public static final String DEBUG_ACCOUNT_FIRST_NAME = "Kydarun";
	/* END of DEBUG */
}
