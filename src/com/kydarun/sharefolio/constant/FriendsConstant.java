package com.kydarun.sharefolio.constant;

public class FriendsConstant {
	public static final String MODULE_PATH = "webscripts/friends.php";
	
	public static final String LOAD_ALL_FRIENDS = "LOAD_ALL_FRIENDS";
	public static final String DELETE_FRIEND = "DELETE_FRIEND";
	public static final String LOAD_ALL_REQUEST = "LOAD_ALL_REQUEST";
	public static final String CONFIRM_FRIEND = "CONFIRM_FRIEND";
	public static final String DELETE_REQUEST = "DELETE_REQUEST";
	public static final String FIND_FRIENDS = "FIND_FRIENDS";
	public static final String REQUEST_FRIEND = "REQUEST_FRIEND";
	public static final String REQUEST_FRIEND_PROFILE = "REQUEST_FRIEND_PROFILE";
	public static final String GENERATE_LINK = "GENERATE_LINK";
	public static final String GENERATE_ALL_FRIENDS = "GENERATE_ALL_FRIENDS";
	public static final String FRIEND_REQUEST_COUNT = "FRIEND_REQUEST_COUNT";
	public static final String GET_FRIEND_STATUS = "GET_FRIEND_STATUS";
	public static final String GET_SINGLE_FRIEND_STATUS = "GET_SINGLE_FRIEND_STATUS";
	public static final String GET_FRIENDS_WITH_FB = "GET_FRIENDS_WITH_FB";
	public static final String GET_FRIENDS_OF_FRIENDS = "GET_FRIENDS_OF_FRIENDS";
	public static final String GET_SUGGESTED_FRIENDS = "GET_SUGGESTED_FRIENDS";
	
	
}
