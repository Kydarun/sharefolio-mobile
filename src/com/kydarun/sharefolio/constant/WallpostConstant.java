package com.kydarun.sharefolio.constant;

public class WallpostConstant {
	public static final String MODULE_URL = "webscripts/wall.php";
	
	public static final String LOAD_FEED = "LOAD_FEED";
	public static final String LOAD_PROFILE_FEED = "LOAD_PROFILE_FEED";
	public static final String USER_LIKE_POST = "USER_LIKE_POST";
	public static final String USER_SUBMIT_COMMENT = "USER_SUBMIT_COMMENT";
	public static final String USER_UNLIKE_POST = "USER_UNLIKE_POST";
	public static final String USER_DELETE_POST = "USER_DELETE_POST";
	public static final String USER_DELETE_COMMENT = "USER_DELETE_COMMENT";
	public static final String LOAD_SINGLE_POST = "LOAD_SINGLE_POST";
	public static final String POST_ON_WALL = "POST_ON_WALL";
	public static final String LOAD_COMPANY_FEED = "LOAD_COMPANY_FEED";
	public static final String LOAD_SINGLE_COMPANY_FEED = "LOAD_SINGLE_COMPANY_FEED";
	public static final String LOAD_MY_COMPANY_FEED = "LOAD_MY_COMPANY_FEED";
	public static final String LOAD_COMPANY_USER_FEED = "LOAD_COMPANY_USER_FEED";
	public static final String POST_ON_COMPANY_WALL = "POST_ON_COMPANY_WALL";
	public static final String LIKE_COMMENT = "LIKE_COMMENT";
	public static final String UNLIKE_COMMENT = "UNLIKE_COMMENT";
	public static final String LOAD_HOME_COMPANY_FEED = "LOAD_HOME_COMPANY_FEED";
	public static final String EDIT_POST = "EDIT_POST";
	public static final String EDIT_COMMENT = "EDIT_COMMENT";
	public static final String LOAD_SINGLE_COMMENT = "LOAD_SINGLE_COMMENT";
	public static final String MAKE_FEATURED_POST = "MAKE_FEATURED_POST";
	public static final String REMOVE_FEATURED = "REMOVE_FEATURED";
	public static final String GET_FEATURED_POST = "GET_FEATURED_POST";
	public static final String LOAD_COMMENTS = "LOAD_COMMENTS";
}
