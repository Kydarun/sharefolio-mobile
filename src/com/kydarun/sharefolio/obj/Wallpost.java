package com.kydarun.sharefolio.obj;

import java.io.Serializable;
import java.util.List;

public class Wallpost implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4597294715559887850L;
	public User profileName;
	public User poster;
	public long wallID;
	public String wallStamp;
	public String message;
	public int totalLikes;
	public int walltype;
	public long secondsGap;
	public String peoplelike;
	public int totalComments;
	public List<Comment> comments;
	public boolean liked;
	
	public static Wallpost findWallpostByID(List<Wallpost> posts, long wallID) {
		for (int i = 0; i < posts.size(); i++) {
			Wallpost post = posts.get(i);
			if (post.wallID == wallID) {
				return post;
			}
		}
		return null;
	}
}
