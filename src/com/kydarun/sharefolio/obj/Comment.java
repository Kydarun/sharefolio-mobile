package com.kydarun.sharefolio.obj;

import java.io.Serializable;
import java.util.List;

public class Comment implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3819169935365815694L;

	public User poster;
	public long commentID;
	public String commentMessage;
	public String commentStamp;
	public int totalLikes;
	public String peopleLikeString;
	public boolean liked;
	public List<User> peopleLike;
	
	public static Comment findCommentByID(List<Comment> comments, long commentID) {
		for (int i = 0; i < comments.size(); i++) {
			Comment comment = comments.get(i);
			if (comment.commentID == commentID) {
				return comment;
			}
		}
		return null;
	}
}
