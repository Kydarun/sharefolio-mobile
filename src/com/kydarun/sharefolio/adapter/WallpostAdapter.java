package com.kydarun.sharefolio.adapter;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.costum.android.widget.PullAndLoadListView;
import com.koushikdutta.urlimageviewhelper.UrlImageViewHelper;
import com.kydarun.sharefolio.R;
import com.kydarun.sharefolio.api.Api;
import com.kydarun.sharefolio.common.GeneralFunctions;
import com.kydarun.sharefolio.constant.ApplicationConstant;
import com.kydarun.sharefolio.constant.WallpostConstant;
import com.kydarun.sharefolio.obj.Comment;
import com.kydarun.sharefolio.obj.User;
import com.kydarun.sharefolio.obj.Wallpost;
import com.kydarun.sharefolio.parser.CommentParser;
import com.kydarun.sharefolio.parser.CommonParser;
import com.kydarun.sharefolio.parser.WallpostParser;

import android.app.Activity;
import android.content.Context;
import android.os.AsyncTask;
import android.view.Display;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.HeaderViewListAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.PopupWindow;
import android.widget.TextView;

public class WallpostAdapter extends BaseAdapter implements OnClickListener, PullAndLoadListView.OnLoadMoreListener, PullAndLoadListView.OnRefreshListener {
	public List<Wallpost> posts;
	public LayoutInflater inflater;
	private Context context;
	public User me;
	private PopupWindow popup;
	private PopupWindow overlay;
	public CommentAdapter commentAdapter;
	private PullAndLoadListView lvComment;
	private Wallpost targetWallpost;
	private boolean isPopupOpened = false;
	
	public WallpostAdapter(Context context, List<Wallpost> posts) {
		this.posts = posts;
		this.context = context;
		inflater = LayoutInflater.from(context);
	}
	
	public WallpostAdapter(Context context, List<Wallpost> posts, User me) {
		this(context, posts);
		this.me = me;
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return posts.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return posts.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return posts.get(position).wallID;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		ViewHolder holder;
		if (convertView == null) {
			convertView = inflater.inflate(R.layout.home_wallpost, null);
			
			holder = new ViewHolder();
			
			holder.ivPosterPicture = (ImageView)convertView.findViewById(R.id.ivPosterPicture);
			holder.tvPosterName = (TextView)convertView.findViewById(R.id.tvPosterName);
			holder.tvWallStamp = (TextView)convertView.findViewById(R.id.tvWallStamp);
			holder.tvPostContent = (TextView)convertView.findViewById(R.id.tvPostContent);
			holder.tvLikeCommentBox = (TextView)convertView.findViewById(R.id.tvLikeCommentBox);
			holder.ivMePicture = (ImageView)convertView.findViewById(R.id.ivMePicture);
			holder.etComment = (EditText)convertView.findViewById(R.id.etComment);
			holder.btnComment = (Button)convertView.findViewById(R.id.btnComment);
			holder.ibLike = (ImageButton)convertView.findViewById(R.id.ibLike);
			holder.tvLike = (TextView)convertView.findViewById(R.id.tvLike);
			
			convertView.setTag(holder);
		}
		else {
			holder = (ViewHolder)convertView.getTag();
		}
		
		Wallpost post = posts.get(position);
		
		if (post != null) {
			String url = "";
			if (ApplicationConstant.DEBUG) {
				url = context.getResources().getString(R.string.debug_url);
			}
			else {
				url = context.getResources().getString(R.string.api_url);
			}
			UrlImageViewHelper.setUrlDrawable(holder.ivPosterPicture, url + "avatar/" + (String)post.profileName.ID + post.profileName.avatar, R.drawable.default_profile);
			UrlImageViewHelper.setUrlDrawable(holder.ivMePicture, url + "avatar/" + (String)me.ID + me.avatar, R.drawable.default_profile);
			holder.tvPosterName.setText(post.profileName.firstname);
			holder.tvPostContent.setText(post.message);
			holder.tvWallStamp.setText(post.wallStamp);
			if (post.totalLikes == 0 && post.totalComments == 0) {
				holder.tvLikeCommentBox.setText("Click here to comment");
			}
			else {
				holder.tvLikeCommentBox.setText(
						(post.totalLikes > 0 ? GeneralFunctions.pluralWord(post.totalLikes, "like") : "") 
						+ " " 
						+ (post.totalComments > 0 ? GeneralFunctions.pluralWord(post.totalComments, "comment") : ""));
			}
			holder.tvLikeCommentBox.setTag("" + post.wallID);
			holder.tvLikeCommentBox.setOnClickListener(this);
			
			holder.btnComment.setTag("" + post.wallID);
			holder.btnComment.setOnClickListener(this);
			holder.ibLike.setTag("" + post.wallID);
			holder.ibLike.setOnClickListener(this);
			holder.tvLike.setTag("" + post.wallID);
			holder.tvLike.setOnClickListener(this);
			if (post.liked) {
				holder.tvLike.setText("Unlike");
			}
			else {
				holder.tvLike.setText("Like");
			}
		}
		
		return convertView;
	}
	
	static class ViewHolder {
		ImageView ivPosterPicture;
		TextView tvPosterName;
		TextView tvWallStamp;
		TextView tvPostContent;
		TextView tvLikeCommentBox;
		ImageView ivMePicture;
		EditText etComment;
		Button btnComment;
		ImageButton ibLike;
		TextView tvLike;
	}

	@Override
	public void onClick(View v) {
		if (v.getId() == R.id.tvLikeCommentBox) {
			popupCommentBox(Long.parseLong((String)v.getTag()), null, true);
		}
		else if (v.getId() == R.id.btnComment) {
			View view = GeneralFunctions.searchChildren(v, R.id.etComment);
			if (view != null) {
				String comment = ((EditText)view).getText().toString();
				if (!comment.equals("")) {
					PostCommentAsyncTask task = new PostCommentAsyncTask(Long.parseLong((String)v.getTag()), comment, v);
					task.execute();
				}
				((EditText)view).setText("");
			}
		}
		else if (v.getId() == R.id.ibPopupLike || v.getId() == R.id.ibLike || v.getId() == R.id.tvLike) {
			Wallpost w = Wallpost.findWallpostByID(posts, Long.parseLong((String)v.getTag()));
			LikePostAsyncTask task = new LikePostAsyncTask(w.wallID, v, w.liked);
			task.execute();
		}
	}
	
	class LoadCommentAsyncTask extends AsyncTask<Void, Void, List<Comment>> {
		private long wallID = 0;
		
		LoadCommentAsyncTask(long wallID) {
			this.wallID = wallID;
		}

		@Override
		protected List<Comment> doInBackground(Void... params) {
			Map<String, String> param = new HashMap<String, String>();
			param.put("accountID", "" + me.ID);
			param.put("wallID", "" + this.wallID);
			param.put("func", WallpostConstant.LOAD_COMMENTS);
			Api request = new Api(context);
			final List<Comment> comments = CommentParser.parseComment(request.postJSON(Api.WALL, param));
			((Activity)context).runOnUiThread(new Runnable() {
				@Override
				public void run() {
					if (lvComment.getAdapter() == null) {
						commentAdapter = new CommentAdapter(context, comments, me);
						lvComment.setAdapter(commentAdapter);
					}
					else {
						commentAdapter.comments = comments;
						((CommentAdapter)((HeaderViewListAdapter)lvComment.getAdapter()).getWrappedAdapter()).notifyDataSetChanged();
					}
				}
			});
			return comments;
		}

		@Override
		protected void onPostExecute(List<Comment> result) {
			lvComment.onRefreshComplete();
			super.onPostExecute(result);
		}
	}
	
	class PostCommentAsyncTask extends AsyncTask<Void, Void, Void> {
		private long wallID = 0;
		private String comments;
		private View vComment;
		
		public PostCommentAsyncTask(long wallID, String comments, View btnComment) {
			this.wallID = wallID;
			this.comments = comments;
			this.vComment = btnComment;
		}

		@Override
		protected Void doInBackground(Void... params) {
			Map<String, String> param = new HashMap<String, String>();
			param.put("func", WallpostConstant.USER_SUBMIT_COMMENT);
			param.put("wallID", "" + this.wallID);
			param.put("accountID", "" + me.ID);
			param.put("userComment", this.comments);
			Api request = new Api(context);
			List<Comment> comments = CommentParser.parseComment(request.postJSON(Api.WALL, param));
			if (comments.size() > 0) {
				((Activity)context).runOnUiThread(new Runnable() {
					@Override
					public void run() {
						popupCommentBox(wallID, vComment, true);
					}
				});
			}	
			return null;
		}
	}
	
	class LikePostAsyncTask extends AsyncTask<Void, Void, Void> {
		private long wallID = 0;
		private View ibLike;
		private boolean liked;
		
		public LikePostAsyncTask(long wallID, View ibLike, boolean liked) {
			this.wallID = wallID;
			this.ibLike = ibLike;
			this.liked = liked;
		}

		@Override
		protected Void doInBackground(Void... params) {
			Map<String, String> param = new HashMap<String, String>();
			if (this.liked) {
				param.put("func", WallpostConstant.USER_UNLIKE_POST);
			}
			else {
				param.put("func", WallpostConstant.USER_LIKE_POST);
			}
			param.put("wallID", "" + this.wallID);
			param.put("accountID", me.ID);
			Api request = new Api(context);
			if (!CommonParser.parseStatus(request.postJSON(Api.WALL, param))) {
				return null;
			}
			this.liked = !this.liked;
			param = new HashMap<String, String>();
			param = new HashMap<String, String>();
			param.put("func", WallpostConstant.LOAD_SINGLE_POST);
			param.put("wallID", "" + this.wallID);
			param.put("accountID", me.ID);
			request = new Api(context);
			final Wallpost post = WallpostParser.parseSingleWallpost(request.postJSON(Api.WALL, param));
			if (CommonParser.parseStatus(request.postJSON(Api.WALL, param))) {
				if (ibLike.getId() == R.id.ibPopupLike) {
					// Reload Like text
					View v = GeneralFunctions.searchChildren(ibLike, R.id.tvCommentPeopleLikes);
					if (v != null) {
						final View tvPeopleLike = v;
						((Activity)context).runOnUiThread(new Runnable() {
							@Override
							public void run() {
								((TextView)tvPeopleLike).setText(post.peoplelike);
							}
						});
					}
				}
				else if (ibLike.getId() == R.id.ibLike || ibLike.getId() == R.id.tvLike) {
					View v = GeneralFunctions.searchChildren(ibLike, R.id.tvLikeCommentBox);
					final TextView tvLikeCommentBox = (TextView)v;
					
					((Activity)context).runOnUiThread(new Runnable() {
						@Override
						public void run() {
							if (ibLike.getId() == R.id.tvLike) {
								if (post.liked) {
									((TextView)ibLike).setText("Unlike");
								}
								else {
									((TextView)ibLike).setText("Like");
								}
							}
							else {
								TextView tvLike = (TextView)GeneralFunctions.searchChildren(ibLike, R.id.tvLike);
								if (post.liked) {
									tvLike.setText("Unlike");
								}
								else {
									tvLike.setText("Like");
								}
							}
							if (post.totalLikes == 0 && post.totalComments == 0) {
								tvLikeCommentBox.setText("Click here to comment");
							}
							else {
								tvLikeCommentBox.setText(
										(post.totalLikes > 0 ? GeneralFunctions.pluralWord(post.totalLikes, "like") : "") 
										+ " " 
										+ (post.totalComments > 0 ? GeneralFunctions.pluralWord(post.totalComments, "comment") : ""));
							}
						}
					});
					
					// Reload wallpost array content
					Wallpost.findWallpostByID(posts, this.wallID).peoplelike = post.peoplelike;
					Wallpost.findWallpostByID(posts, this.wallID).liked = this.liked;
				}
			}
			return null;
		}
	}

	
	@SuppressWarnings("deprecation")
	private void popupCommentBox(long wallID, View vComment, boolean isScrollToLast) {
		try {
			View popupLayout = null;
			if (!isPopupOpened) {
				isPopupOpened = true;
				
				popupLayout = inflater.inflate(R.layout.comment_popup, null);
				View overlayLayout = inflater.inflate(R.layout.popup_overlay, null);
				Display d = ((WindowManager)context.getSystemService(Context.WINDOW_SERVICE)).getDefaultDisplay();
				overlay = new PopupWindow(overlayLayout, d.getWidth(), d.getHeight(), true);
				overlay.showAtLocation(overlayLayout, Gravity.CENTER, 0, 0);
				popup = new PopupWindow(popupLayout, d.getWidth() - 100, d.getHeight() - 150, true);
				popup.showAtLocation(popupLayout, Gravity.CENTER, 0, 0);
				
				Button ibClose = (Button)popupLayout.findViewById(R.id.ibClose);
				ibClose.setOnClickListener(new OnClickListener() {
					@Override
					public void onClick(View v) {
						popup.dismiss();
						overlay.dismiss();
						isPopupOpened = false;
					}
				});
				
				lvComment = (PullAndLoadListView)popupLayout.findViewById(R.id.lvComment);
				View vEmptyComment = popupLayout.findViewById(R.id.empty_comment);
				lvComment.setEmptyView(vEmptyComment);
				
				lvComment.setOnLoadMoreListener(this);
				lvComment.setOnRefreshListener(this);
			}
			else {
				popupLayout = (View)vComment.getParent();
			}
			
			targetWallpost = Wallpost.findWallpostByID(posts, wallID);
			
			if (targetWallpost.peoplelike != null) {
				TextView tvCommentPeopleLike = (TextView)popupLayout.findViewById(R.id.tvCommentPeopleLikes);
				tvCommentPeopleLike.setText(targetWallpost.peoplelike);
			}
			ImageView ivMePicture = (ImageView)popupLayout.findViewById(R.id.ivMePicture);
			String url;
			if (ApplicationConstant.DEBUG) {
				url = context.getResources().getString(R.string.debug_url);
			}
			else {
				url = context.getResources().getString(R.string.api_url);
			}
			UrlImageViewHelper.setUrlDrawable(ivMePicture, url + "avatar/" + me.ID + me.avatar, R.drawable.default_profile);
			
			LoadCommentAsyncTask task = new LoadCommentAsyncTask(targetWallpost.wallID);
			task.execute();
			
			Button btnComment = (Button)popupLayout.findViewById(R.id.btnComment);
			btnComment.setTag("" + wallID);
			if (btnComment != null) {
				btnComment.setOnClickListener(this);
			}
			
			ImageButton ibPopupLike = (ImageButton)popupLayout.findViewById(R.id.ibPopupLike);
			ibPopupLike.setTag("" + targetWallpost.wallID);
			if (ibPopupLike != null) {
				ibPopupLike.setOnClickListener(this);
			}
			
			if (isScrollToLast) {
				lvComment.setSelection(commentAdapter.getCount());
			}
		}
		catch (Exception ex) {
			ex.printStackTrace();
		}
	}

	@Override
	public void onRefresh() {
		LoadCommentAsyncTask task = new LoadCommentAsyncTask(targetWallpost.wallID);
		task.execute();
	}

	@Override
	public void onLoadMore() {
		
	}
}
