package com.kydarun.sharefolio.adapter;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.koushikdutta.urlimageviewhelper.UrlImageViewHelper;
import com.kydarun.sharefolio.R;
import com.kydarun.sharefolio.api.Api;
import com.kydarun.sharefolio.common.GeneralFunctions;
import com.kydarun.sharefolio.constant.ApplicationConstant;
import com.kydarun.sharefolio.constant.WallpostConstant;
import com.kydarun.sharefolio.obj.Comment;
import com.kydarun.sharefolio.obj.User;
import com.kydarun.sharefolio.parser.CommentParser;

import android.app.Activity;
import android.content.Context;
import android.os.AsyncTask;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

public class CommentAdapter extends BaseAdapter implements OnClickListener {
	public List<Comment> comments;
	public LayoutInflater inflater;
	private Context context;
	public User me;
	
	public CommentAdapter(Context context, List<Comment> comments) {
		this.comments = comments;
		this.context = context;
		this.inflater = LayoutInflater.from(context);
	}
	
	public CommentAdapter(Context context, List<Comment> comments, User me) {
		this(context, comments);
		this.me = me;
	}
	
	@Override
	public int getCount() {
		return comments.size();
	}

	@Override
	public Object getItem(int position) {
		return comments.get(position);
	}

	@Override
	public long getItemId(int position) {
		return comments.get(position).commentID;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		ViewHolder holder;
		if (convertView == null) {
			convertView = inflater.inflate(R.layout.comment_item, null);
			
			holder = new ViewHolder();
			
			holder.ivCommentPicture = (ImageView)convertView.findViewById(R.id.ivCommentPicture);
			holder.tvCommentName = (TextView)convertView.findViewById(R.id.tvCommentName);
			holder.tvComments = (TextView)convertView.findViewById(R.id.tvComments);
			holder.tvCommentStamp = (TextView)convertView.findViewById(R.id.tvCommentStamp);
			holder.tvNumberOfLikes = (TextView)convertView.findViewById(R.id.tvNumberOfLikes);
			holder.ibCommentLike = (ImageButton)convertView.findViewById(R.id.ibCommentLike);
			holder.tvCommentLike = (TextView)convertView.findViewById(R.id.tvCommentLike);
			
			convertView.setTag(holder);
		}
		else {
			holder = (ViewHolder)convertView.getTag();
		}
		
		Comment comment = comments.get(position);
		
		if (comment != null) {
			String url = "";
			if (ApplicationConstant.DEBUG) {
				url = context.getResources().getString(R.string.debug_url);
			}
			else {
				url = context.getResources().getString(R.string.api_url);
			}
			UrlImageViewHelper.setUrlDrawable(holder.ivCommentPicture, url + "avatar/" + comment.poster.ID + comment.poster.avatar, R.drawable.default_profile);
			holder.tvCommentName.setText(comment.poster.firstname);
			holder.tvComments.setText(comment.commentMessage.replace("<br />", ""));
			holder.tvCommentStamp.setText(comment.commentStamp);
			if (comment.totalLikes > 0) {
				holder.tvNumberOfLikes.setText("" + comment.totalLikes);
			}
			holder.ibCommentLike.setTag("" + comment.commentID);
			holder.ibCommentLike.setOnClickListener(this);
			holder.tvCommentLike.setTag("" + comment.commentID);
			holder.tvCommentLike.setOnClickListener(this);
		}
		
		return convertView;
	}

	static class ViewHolder {
		ImageView ivCommentPicture;
		TextView tvCommentName;
		TextView tvComments;
		TextView tvCommentStamp;
		TextView tvNumberOfLikes;
		ImageButton ibCommentLike;
		TextView tvCommentLike;
	}

	@Override
	public void onClick(View v) {
		if (v.getId() == R.id.ibCommentLike || v.getId() == R.id.tvCommentLike) {
			Comment comment = Comment.findCommentByID(comments, Long.parseLong((String)v.getTag()));
			LikeCommentAsyncTask task = new LikeCommentAsyncTask(comment.commentID, v);
			task.execute();
		}
	}
	
	class LikeCommentAsyncTask extends AsyncTask<Void, Void, Void> {
		private long commentID;
		private View v;
		
		public LikeCommentAsyncTask(long commentID, View v) {
			this.commentID = commentID;
			this.v = v;
		}
		
		@Override
		protected Void doInBackground(Void... params) {
			Map<String, String> param = new HashMap<String, String>();
			
			if (Comment.findCommentByID(comments, commentID).liked) {
				param.put("func", WallpostConstant.UNLIKE_COMMENT);
			}
			else {
				param.put("func", WallpostConstant.LIKE_COMMENT);
			}
			param.put("commentID", "" + this.commentID);
			param.put("accountID", me.ID);
			Api request = new Api(context);
			final Comment comment = CommentParser.parseSingleComment(request.postJSON(Api.WALL, param));
			Comment.findCommentByID(comments, commentID).liked = !Comment.findCommentByID(comments, commentID).liked;
			((Activity)context).runOnUiThread(new Runnable() {
				@Override
				public void run() {
					final TextView tvNumberOfLikes = (TextView)GeneralFunctions.searchChildren(v, R.id.tvNumberOfLikes);
					TextView tvCommentLike = null;
					if (v.getId() == R.id.ibCommentLike) {
						tvCommentLike = (TextView)GeneralFunctions.searchChildren(v, R.id.tvCommentLike);
					}
					else {
						tvCommentLike = (TextView)v;
					}
					
					// Change number of likes
					tvNumberOfLikes.setText("" + comment.totalLikes);
					
					// Change the "like" text
					if (Comment.findCommentByID(comments, commentID).liked) {
						tvCommentLike.setText("Unlike");
					}
					else {
						tvCommentLike.setText("Like");
					}
					
					// Update the array
					Comment.findCommentByID(comments, commentID).liked = comment.liked;
					Comment.findCommentByID(comments, commentID).peopleLikeString = comment.peopleLikeString;
					Comment.findCommentByID(comments, commentID).totalLikes = comment.totalLikes;
				}
			});
			return null;
		}
	}
}
