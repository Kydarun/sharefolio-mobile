package com.kydarun.sharefolio.common;

import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Random;

import org.apache.commons.codec.binary.Base64;

import android.graphics.drawable.Drawable;
import android.view.View;
import android.view.ViewGroup;

public class GeneralFunctions {
	public static String pluralWord(int count, String word) {
		return count > 1 ? count + " " + word + "s" : count + " " + word;
	}
	
	public static Drawable loadBackground(String url) {
		try {
			InputStream is = (InputStream)GeneralFunctions.fetchFromServer(url);
			Drawable d = Drawable.createFromStream(is, "avatar.png");
			return d;
		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		return null;
	}
	
	public static View searchChildren(View source, int target) {
		ViewGroup parent = (ViewGroup)source.getParent();
		for (int i = 0; i < parent.getChildCount(); i++) {
			View v = parent.getChildAt(i);
			if (v.getId() == target) {
				return v;
			}
		}
		
		return null;
	}
	
	private static Object fetchFromServer(String address) throws MalformedURLException, IOException {
		URL url = new URL(address);
		Object content = url.getContent();
		return content;
	}
	
	public static String encrypt(String plainText) {
		Random r = new Random();
		byte[] firstPart = Base64.encodeBase64(((11111111 + r.nextInt(88888888)) + "").getBytes());
		byte[] secondPart = Base64.encodeBase64(((11111111 + r.nextInt(88888888)) + "").getBytes());
		byte[] encodedBytes = Base64.encodeBase64(plainText.getBytes());
		return new String(firstPart) + new String(encodedBytes) + new String(secondPart);
	}
	
	public static String decrypt(String cipherText) {
		String accountID = cipherText.substring(0, 12);
		accountID = accountID.substring(0, accountID.length() - 12);
		return accountID;
	}
}
