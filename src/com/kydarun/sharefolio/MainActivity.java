package com.kydarun.sharefolio;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;

import com.costum.android.widget.PullAndLoadListView;
import com.koushikdutta.urlimageviewhelper.UrlImageViewHelper;
import com.kydarun.sharefolio.adapter.WallpostAdapter;
import com.kydarun.sharefolio.api.Api;
import com.kydarun.sharefolio.common.GeneralFunctions;
import com.kydarun.sharefolio.constant.ApplicationConstant;
import com.kydarun.sharefolio.constant.WallpostConstant;
import com.kydarun.sharefolio.obj.User;
import com.kydarun.sharefolio.obj.Wallpost;
import com.kydarun.sharefolio.parser.WallpostParser;
import android.os.AsyncTask;
import android.os.Bundle;
import android.app.Activity;
import android.content.Context;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.HeaderViewListAdapter;
import android.widget.ImageView;

public class MainActivity extends Activity implements PullAndLoadListView.OnLoadMoreListener, PullAndLoadListView.OnRefreshListener, OnClickListener {
	private Context context;
	private WallpostAdapter adapter;
	private PullAndLoadListView lvWallpost;
	private List<Wallpost> posts = new ArrayList<Wallpost>();
	private HomeWallpostAsyncTask task;
	private User me;
	
	private int startingPoint = 0;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		super.onCreate(savedInstanceState);
		this.context = this;
		setContentView(R.layout.activity_main);
		
		me = new User();
		if (ApplicationConstant.DEBUG) {
			me.ID = "" + ApplicationConstant.DEBUG_ACCOUNT_ID;
			me.avatar = ApplicationConstant.DEBUG_ACCOUNT_AVATAR;
			me.firstname = ApplicationConstant.DEBUG_ACCOUNT_FIRST_NAME;
		}
		else {
			// Leave it blank for the time being
		}
		
		lvWallpost = (PullAndLoadListView)this.findViewById(R.id.lvWallpost);
		lvWallpost.setOnRefreshListener(this);
		lvWallpost.setOnLoadMoreListener(this);
		task = new HomeWallpostAsyncTask();
		task.execute();
		
		ImageView ivProfile = (ImageView)findViewById(R.id.ivProfile);
		String ivProfileURL = "";
		if (ApplicationConstant.DEBUG) {
			ivProfileURL = getResources().getString(R.string.debug_url) + "avatar/" + me.ID + ApplicationConstant.DEBUG_ACCOUNT_AVATAR;
		}
		else {
			ivProfileURL = getResources().getString(R.string.api_url);
		}
		UrlImageViewHelper.setUrlDrawable(ivProfile, ivProfileURL, R.drawable.default_profile);
		
		Button btnNewPost = (Button)this.findViewById(R.id.btnNewPost);
		if (btnNewPost != null) {
			btnNewPost.setOnClickListener(this);
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}
	
	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
	}

	class HomeWallpostAsyncTask extends AsyncTask<Void, Void, List<Wallpost>> {
		private int sp = 0;
		
		public HomeWallpostAsyncTask(int sp) {
			this.sp = sp;
		}
		
		public HomeWallpostAsyncTask() {
			this(0);
		}

		@Override
		protected List<Wallpost> doInBackground(Void... params) {
			Map<String, String> param = new Hashtable<String, String>();
			param.put("accountID", "" + me.ID);
			param.put("func", WallpostConstant.LOAD_FEED);
			param.put("limitLoad", "15");
			param.put("startingPoint", "" + this.sp);
			Api request = new Api(context);
			posts = WallpostParser.parseWallpost(request.postJSON(Api.WALL, param));
			if (posts != null) {
				((Activity)context).runOnUiThread(new Runnable() {
					@Override
					public void run() {
						if (lvWallpost.getAdapter() == null || sp == 0) {
							adapter = new WallpostAdapter(context, posts, me);
							lvWallpost.setAdapter(adapter);
						}
						else {
							adapter.posts.addAll(posts);
							((WallpostAdapter)((HeaderViewListAdapter)lvWallpost.getAdapter()).getWrappedAdapter()).notifyDataSetChanged();
						}
					}
				});
			}
			return posts;
		}

		@Override
		protected void onPostExecute(List<Wallpost> result) {
			lvWallpost.onRefreshComplete();
			super.onPostExecute(result);
		}
	}
	
	class PostOnWallAsyncTask extends AsyncTask<Void, Void, Void> {
		private String message;
		
		public PostOnWallAsyncTask(String message) {
			this.message = message;
		}
		
		@Override
		protected Void doInBackground(Void... params) {
			Map<String, String> param = new HashMap<String, String>();
			param.put("func", WallpostConstant.POST_ON_WALL);
			param.put("accountID", me.ID);
			param.put("messageStatus", this.message);
			Api request = new Api(context);
			List<Wallpost> post = WallpostParser.parseWallpost(request.postJSON(Api.WALL, param));
			adapter.posts.add(0, post.get(0));
			((Activity)context).runOnUiThread(new Runnable() {
				@Override
				public void run() {
					((WallpostAdapter)((HeaderViewListAdapter)lvWallpost.getAdapter()).getWrappedAdapter()).notifyDataSetChanged();
				}
			});
			return null;
		}
	}

	@Override
	public void onRefresh() {
		startingPoint = 0;
		task = new HomeWallpostAsyncTask(startingPoint);
		task.execute();
	}

	@Override
	public void onLoadMore() {
		this.startingPoint += 15;
		task = new HomeWallpostAsyncTask(startingPoint);
		task.execute();
	}

	@Override
	public void onClick(View v) {
		if (v.getId() == R.id.btnNewPost) {
			EditText etNewPost = (EditText)GeneralFunctions.searchChildren(v, R.id.etNewPost);
			PostOnWallAsyncTask task = new PostOnWallAsyncTask(etNewPost.getText().toString());
			task.execute();
			etNewPost.setText("");
		}
	}
}
