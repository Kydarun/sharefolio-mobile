package com.kydarun.sharefolio.parser;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.kydarun.sharefolio.obj.User;
import com.kydarun.sharefolio.obj.Wallpost;

public class WallpostParser {
	public static List<Wallpost> parseWallpost(JSONObject json) {
		if (json.has("wallpost")) {
			List<Wallpost> posts = new ArrayList<Wallpost>();
			try {
				JSONArray wallposts = json.getJSONArray("wallpost");
				for (int i = 0; i < wallposts.length(); i++) {
					JSONObject wallpost = wallposts.getJSONObject(i);
					Wallpost post = new Wallpost();
					User profileName = new User();
					profileName.ID = wallpost.getJSONObject("profilename").getString("id");
					profileName.firstname = wallpost.getJSONObject("profilename").getString("firstname");
					profileName.avatar = wallpost.getString("profileimage");
					post.profileName = profileName;
					post.wallID = wallpost.getInt("wallid");
					post.totalLikes = wallpost.getInt("totallikes");
					if (wallpost.has("commentcount")) {
						post.totalComments = wallpost.getInt("commentcount");
					}
					else {
						post.totalComments = 0;
					}
					post.walltype = wallpost.getInt("walltype");
					post.secondsGap = wallpost.getLong("secondsgap");
					post.message = wallpost.getString("message").replace("<br />", "");
					post.wallStamp = wallpost.getString("timestamp");
					post.liked = wallpost.getBoolean("liked");
					User poster = new User();
					if (wallpost.get("poster") instanceof JSONObject) {
						poster.ID = wallpost.getJSONObject("poster").getString("posterid");
						poster.firstname =  wallpost.getJSONObject("poster").getString("postername");
					}
					else if (wallpost.get("poster") instanceof JSONArray) {
						if (wallpost.getJSONArray("poster").length() > 0) {
							poster.ID = wallpost.getJSONObject("poster").getString("posterid");
							poster.firstname =  wallpost.getJSONObject("poster").getString("postername");
						}
					}
					post.poster = poster;
					if (wallpost.has("peoplelike")) {
						post.peoplelike = wallpost.getString("peoplelike");
					}
					if (wallpost.has("comment")) {
						post.comments = CommentParser.parseComment(wallpost);
					}
					posts.add(post);
				}
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return posts;
		}
		
		return new ArrayList<Wallpost>();
	}
	
	public static Wallpost parseSingleWallpost(JSONObject json) {
		if (json.has("wallpost")) {
			Wallpost post = new Wallpost();
			try {
				JSONObject jsonWall = json.getJSONObject("wallpost");
				User profileName = new User();
				profileName.ID = jsonWall.getJSONObject("profilename").getString("id");
				profileName.firstname = jsonWall.getJSONObject("profilename").getString("firstname");
				profileName.avatar = jsonWall.getString("profileimage");
				post.profileName = profileName;
				post.wallID = jsonWall.getInt("wallid");
				post.totalLikes = jsonWall.getInt("totallikes");
				post.totalComments = jsonWall.getInt("commentcount");
				post.walltype = jsonWall.getInt("walltype");
				post.secondsGap = jsonWall.getLong("secondsgap");
				post.message = jsonWall.getString("message").replace("<br />", "");
				post.wallStamp = jsonWall.getString("timestamp");
				post.liked = jsonWall.getBoolean("liked");
				User poster = new User();
				if (jsonWall.get("poster") instanceof JSONObject) {
					poster.ID = jsonWall.getJSONObject("poster").getString("posterid");
					poster.firstname =  jsonWall.getJSONObject("poster").getString("postername");
				}
				else if (jsonWall.get("poster") instanceof JSONArray) {
					if (jsonWall.getJSONArray("poster").length() > 0) {
						poster.ID = jsonWall.getJSONObject("poster").getString("posterid");
						poster.firstname =  jsonWall.getJSONObject("poster").getString("postername");
					}
				}
				post.poster = poster;
				if (jsonWall.has("peoplelike")) {
					post.peoplelike = jsonWall.getString("peoplelike");
				}
				if (jsonWall.has("comment")) {
					post.comments = CommentParser.parseComment(jsonWall);
				}
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return post;
		}
		
		return null;
	}
}
