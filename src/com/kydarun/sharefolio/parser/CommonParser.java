package com.kydarun.sharefolio.parser;

import org.json.JSONObject;

public class CommonParser {
	public static boolean parseStatus(JSONObject json) {
		if (json.has("status")) {
			try {
				if (json.getString("status").equals("ok")) {
					return true;
				}
				else {
					return false;
				}
			}
			catch (Exception ex) {
				ex.printStackTrace();
			}
		}
		return false;
	}
}
