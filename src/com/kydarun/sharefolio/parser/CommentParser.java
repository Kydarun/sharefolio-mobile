package com.kydarun.sharefolio.parser;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.kydarun.sharefolio.obj.Comment;
import com.kydarun.sharefolio.obj.User;

public class CommentParser {
	public static List<Comment> parseComment(JSONObject json) {
		if (json.has("comment")) {
			List<Comment> comments = new ArrayList<Comment>();
			try {
				JSONArray jsonComments = json.getJSONArray("comment");
				for (int i = jsonComments.length() - 1; i >= 0; i--) {
					Comment comment = new Comment();
					User poster = new User();
					JSONObject jsonComment = jsonComments.getJSONObject(i);
					poster.ID = jsonComment.getString("id");
					poster.firstname = jsonComment.getString("firstname");
					poster.avatar = jsonComment.getString("commentavatar");
					comment.commentID = jsonComment.getLong("commentid");
					comment.poster = poster;
					comment.commentMessage = jsonComment.getString("commentmsg");
					if (jsonComment.has("commentlikecount")) {
						comment.totalLikes = jsonComment.getInt("commentlikecount");
					}
					else {
						comment.totalLikes = 0;
					}
					if (jsonComment.has("commentlike")) {
						comment.peopleLikeString = jsonComment.getString("commentlike");
					}
					else {
						comment.peopleLikeString = "";
					}
					comment.liked = jsonComment.getBoolean("liked");
					comment.commentStamp = jsonComment.getString("timestamp");
					
					comments.add(comment);
				}
				
				return comments;
			} catch (JSONException e) {
				e.printStackTrace();
			}
		}
		return new ArrayList<Comment>();
	}
	
	public static Comment parseSingleComment(JSONObject json) {
		if (json.has("comment")) {
			try {
				Comment comment = new Comment();
				
				User poster = new User();
				JSONObject jsonComment = json.getJSONObject("comment");
				poster.ID = jsonComment.getString("id");
				poster.firstname = jsonComment.getString("firstname");
				poster.avatar = jsonComment.getString("commentavatar");
				comment.commentID = jsonComment.getLong("commentid");
				comment.poster = poster;
				comment.commentMessage = jsonComment.getString("commentmsg");
				if (jsonComment.has("commentlikecount")) {
					comment.totalLikes = jsonComment.getInt("commentlikecount");
				}
				else {
					comment.totalLikes = 0;
				}
				if (jsonComment.has("commentlike")) {
					comment.peopleLikeString = jsonComment.getString("commentlike");
				}
				else {
					comment.peopleLikeString = "";
				}
				comment.liked = jsonComment.getBoolean("liked");
				comment.commentStamp = jsonComment.getString("timestamp");
				
				return comment;
			}
			catch (Exception ex) {
				ex.printStackTrace();
			}
		}
		else {
			try {
				Comment comment = new Comment();
				comment.peopleLikeString = json.getString("commentlike");
				comment.totalLikes = json.getInt("commentlikecount");
				comment.liked = json.getBoolean("liked");
				
				return comment;
			}
			catch (Exception ex) {
				ex.printStackTrace();
			}
		}
		
		return null;
	}
}
